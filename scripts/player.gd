extends Node2D

var sounds_lib = ["relic", "teleporter", "key" ]

func _ready():
	# Sounds
	var samplep = SamplePlayer2D.new()
	
	samplep.set_name("sound")
	var lib = SampleLibrary.new()
	for sound in sounds_lib:
		lib.add_sample( sound, ResourceLoader.load("res://assets/common/sounds/"+sound+".wav") )
	samplep.set_sample_library( lib )
	add_child(samplep)
	
	# Music
	var music = StreamPlayer.new()
	add_child(music)
	music.set_stream( ResourceLoader.load("res://assets/common/music/bg.ogg") )
	music.play()
	music.set_loop(true)
	music.set_volume( 0.2 )

var death = 0
var controls_enabled = true